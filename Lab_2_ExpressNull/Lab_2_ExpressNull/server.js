const express = require('express')
const app = express()
const port = 1337
const url = 'https://www.cbr-xml-daily.ru/daily_json.js';
const req = require("request")

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.get("/", function (request, response) {

    response.send("<h2>3anpoc</h2>");

});

app.use("/get_exchange_rate", function (request, respons, next) {

    let char = request.query.char_code;


    req(url, function (error, response, body) {
        try {
            var value = JSON.parse(response.body).Valute[char].Value
            respons.send('Value: ' + value.toString())
        }
        catch (err) {
            respons.send("Error")
        }
    })
})