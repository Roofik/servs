const { app, BrowserWindow } = require('electron')
const req1 = require("request")
const req2 = require("request")
const url1 = "https://www.cbr-xml-daily.ru/daily_json.js";
const exp = require('express')
const express = exp()
const port = 1337

function createWindow() {
    // Create the browser window.
    let win = new BrowserWindow({
        width: 1600,
        height: 1000,
        webPreferences: {
            nodeIntegration: true
        }
    })

    // and load the index.html of the app.
    win.loadFile('index.html');
    win.webContents.openDevTools();
}

express.listen(port, () => console.log(`Example app listening on port ${port}!`))

express.get("/get_exchange_rate", function (request, response) {
    var char = request.query.char_code;
    var date1 = request.query.date1;
    var date2 = request.query.date2;
    console.log(char, date1, date2);

    req1(url1, function (error, respons, body) {
        try {
            var id = JSON.parse(respons.body).Valute[char].ID;
            console.log(id);

            var url2 = "http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=";

            url2 += date1 + "&date_req2=" + date2 + "&VAL_NM_RQ=" + id;

            req2(url2, function (error, respons2, body) {
                const js = require("jsdom");
                var xml = new js.JSDOM(body);

                var data = [];
                data.push(["Date", "Valute value"]);
                var arr = xml.window.document.querySelectorAll("Record");

                for (var i = 0; i < arr.length; i++) {
                    var date = arr[i].getAttribute("Date")
                    var value = arr[i].querySelector('Value').innerHTML
                    data.push([date, parseFloat(value.replace(/[,]/g, "."))])
                }

                response.send(JSON.stringify(data));
            })
        }
        catch (err) {
            response.send("Error");
        }
    })
})

app.on('ready', createWindow);