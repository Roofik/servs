const { app, BrowserWindow } = require('electron')
const req1 = require("request")
const req2 = require("request")
const url = "https://www.cbr-xml-daily.ru/daily_json.js"
const exp = require('express')
const express = exp()
const port = 1337

function createWindow() {
    // Create the browser window.
    let win = new BrowserWindow({
        width: 1600,
        height: 1000,
        webPreferences: {
            nodeIntegration: true
        }
    })

    // and load the index.html of the app.
    win.loadFile('index.html');
    win.webContents.openDevTools();
}

express.listen(port, () => console.log(`Example app listening on port ${port}!`))

express.get("/", function (request, respons) {
    respons.send("")
})

express.get("/get_exchange_rate", function (request, respons) {
    var char = request.query.char_code;
    console.log(char);
    //name = request.query.name_valute.toString().replace(/[_]/g, " ");
    if (typeof char != "undefined") {
        var value;
        req1(url, function (error, response, body) {
            try {
                value = JSON.parse(response.body).Valute[char].Value
                respons.send(value.toString())
            }
            catch (err) {
                respons.send("Error")
            }
        })
    }

    var valute = request.query.name_valute;
    console.log(valute);

    if (typeof valute != 'undefined') {

        var jsonFile = null;
        jsonElem = {};

        req1(url, function (error, response, body) {
            try {
                jsonFile = JSON.parse(response.body);
                for (var e in jsonFile.Valute) {
                    jsonElem[jsonFile.Valute[e].Name] = jsonFile.Valute[e].Value.toString();
                }
                respons.send(jsonElem[valute])
            }
            catch (err) {
                respons.send("Error")
            }
        })
    }
})


app.on('ready', createWindow)